# Use args so we can build specific versions
ARG DOCKER_VERSION

FROM docker:${DOCKER_VERSION}

ARG COMPOSE_VERSION

RUN apk add --no-cache python3-dev py3-cryptography curl bash git py3-pip py3-wheel py3-yaml py3-cryptography py3-bcrypt py3-pynacl py3-paramiko py3-dotenv py3-texttable py3-pyrsistent py3-jsonschema py3-docopt \
	&& pip3 install cryptography docker-compose==${COMPOSE_VERSION} requests \
	&& curl -sL https://sentry.io/get-cli/ | bash


